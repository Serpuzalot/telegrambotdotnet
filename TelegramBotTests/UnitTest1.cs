using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Telegram.Bot.Examples.Polling;

namespace TelegramBotTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ReturnCurrency_01dot01dotYearNow_EUEE_errorReturned()
        {
            //arrange
            int year = DateTime.Now.Year;
            string input = "01.01." + year.ToString()+ " EUEE";
            string expected = "Incorrect input currency code . Try again!";

            //act
            PrivatBankApi api = new PrivatBankApi();
            string actual = api.ReturnCurrency(input);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnCurrency_01dot01dotYearNow_eur_notErrorReturned()
        {
            //arrange
            int year = DateTime.Now.Year;
            string input = "01.01." + year.ToString()+" eur";
            bool expected = true;

            //act
            PrivatBankApi api = new PrivatBankApi();
            string amsver = api.ReturnCurrency(input);
            bool actual = amsver.Contains("Date");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnCurrency_01dot01dotYearNow_EUR_notErrorReturned()
        {

            //arrange
            int year = DateTime.Now.Year;
            string input = "01.01." + year.ToString() + " EUR";
            bool expected = true;

            //act
            PrivatBankApi api = new PrivatBankApi();
            string ansver = api.ReturnCurrency(input);
            bool actual = ansver.Contains("Date");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnCurrency_0101dotYearNow_EUR_ErrorReturned()
        {
            //arrange
            int year = DateTime.Now.Year;
            string input = "0101." + year.ToString()+" EUR";
            string expected = "Incorrect date input . Try again!";

            //act
            PrivatBankApi api = new PrivatBankApi();
            string actual = api.ReturnCurrency(input);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void ReturnCurrency_01dot01dotYearNowdot_EUR_ErrorReturned()
        {
            //arrange
            int year = DateTime.Now.Year;
            string input = "01.01." + year.ToString()+". EUR";
            string expected = "Incorrect date input . Try again!";

            //act
            PrivatBankApi api = new PrivatBankApi();
            string actual = api.ReturnCurrency(input);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void ReturnCurrency_01dot1dotNowYeardot_notErrorReturned()
        {
            //arrange
            int year = DateTime.Now.Year;
            string input = "01.1." + year.ToString() + " EUR";
            bool expected = true;

            //act
            PrivatBankApi api = new PrivatBankApi();
            string ansver = api.ReturnCurrency(input);
            bool actual = ansver.Contains("Date");

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ReturnCurrency_01dot40dotNowYear_minus1_EUR_errorReturned()
        {
            //arrange
            int year = DateTime.Now.Year-1;
            string input = "01.40." + year.ToString() + " EUR";
            string expected = "Incorrect date input . Try again!";

            //act
            PrivatBankApi api = new PrivatBankApi();
            string actual = api.ReturnCurrency(input);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void ReturnCurrency_40dot01dotNowYear_minus1_EUR_errorReturned()
        {
            //arrange
            int year = DateTime.Now.Year - 1;
            string input = "40.01." + year.ToString() + " EUR";
            string expected = "Incorrect date input . Try again!";

            //act
            PrivatBankApi api = new PrivatBankApi();
            string actual = api.ReturnCurrency(input);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void ReturnCurrency_01dot01dot1999_EUR_errorReturned()
        {
            //arrange
            int year = 1999;
            string input = "01.01." + year.ToString() + " EUR";
            string expected = "Incorrect date input . Try again!";

            //act
            PrivatBankApi api = new PrivatBankApi();
            string actual = api.ReturnCurrency(input);

            //assert
            Assert.AreEqual(expected, actual);

        }
    }
}
