using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

 namespace Telegram.Bot.Examples.Polling
{
    public class BankInfo
    {
        public string date { get; set; }
        public string bank { get; set; }
        public int baseCurrency { get; set; }
        public string baseCurrencyLit { get; set; }
        public IList<MoneyModel> exchangeRate { get; set; }
    }

    public class MoneyModel
    {
        public string baseCurrency { get; set; }
        public string currency { get; set; }
        public double saleRateNB { get; set; }
        public double purchaseRateNB { get; set; }
        public double saleRate { get; set; }
        public double purchaseRate { get; set; }

        public string ToString()
        {
            return $"Currency: {currency} \nSaleRateNB: {saleRateNB} \nPurchaseRateNB: {purchaseRateNB} \nSaleRate: {saleRate} \nPurchaseRate {purchaseRate}";
        }
    }
}
