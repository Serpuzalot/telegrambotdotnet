using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Telegram.Bot.Examples.Polling
{
    public class PrivatBankApi
    {
        static WebRequest _request;
        static HttpWebResponse _response;
        private static int _MinYear = DateTime.Now.Year-4;
        private string _apiRequest = "https://api.privatbank.ua/p24api/exchange_rates?json&date=";
        private List<MoneyModel> _apiMoneyInfo = new List<MoneyModel>();


        private void GetDataFromApi(string data)
        {
            string correctData = DateTime.Parse(data).ToShortDateString();
            string requestLine = _apiRequest + correctData;
            _request = WebRequest.Create(requestLine);
            _response = (HttpWebResponse)_request.GetResponse();
            string line = "";
            using (Stream stream = _response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    line = reader.ReadLine();
                }
            }



            _response.Close();
            BankInfo bankInfo = JsonSerializer.Deserialize<BankInfo>(line);
            foreach (var el in bankInfo.exchangeRate)
            {
                _apiMoneyInfo.Add(el);
            }

        }

        private bool CheckMoneyCode(string date, string currencyCode)
        {
            string upperCode = currencyCode.ToUpper();
            GetDataFromApi(date);
            foreach (var el in _apiMoneyInfo)
            {
                if (el.currency == upperCode)
                {
                    return true;
                }
            }
            return false;
        }

        private bool CheckDataFormat(string data)
        {
            DateTime result;
            if (DateTime.TryParse(data,out result)){
                if(DateTime.Now.Year == result.Year)
                {
                    if (result.Month < DateTime.Now.Month)
                    {
                        return true;
                    }
                    else if (result.Month == DateTime.Now.Month && result.Day <= DateTime.Now.Day)
                    {
                        return true;
                    }
                    return false;

                }
                return true;
            }
            return false;
        }

        public string ReturnCurrency(string inputContent)
        {
            string[] parts = inputContent.Split(" ");
            if(parts.Length != 2)
            {
                return "Incorrect input currence code and date . Try again!";
            }
            if (CheckDataFormat(parts[0]))
            {
                if(DateTime.Parse(parts[0]).Year < _MinYear)
                {
                    return "Minimum allowable year -4 years from the current";
                }
                if (CheckMoneyCode(parts[0],parts[1]))
                {
                    return GetCurrencyByDate(parts[0], parts[1]);
                }
                else
                {
                    return "Incorrect input currency code . Try again!";
                }
            }
            else
            {
                return "Incorrect date input . Try again!";
            }
        }

        private string GetCurrencyByDate(string date, string currencyCode)
        {
            string result = "";
            StringBuilder sb = new StringBuilder();
            foreach (var el in _apiMoneyInfo)
            {
                if (el.currency == currencyCode.ToUpper())
                {
                    sb.Append($"Date:{date}\n");
                    sb.AppendLine(el.ToString());
                    break;
                }
            }
            result = sb.ToString();
            return result;
        }

    }
}
